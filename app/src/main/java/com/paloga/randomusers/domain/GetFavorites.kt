package com.paloga.randomusers.domain

import com.paloga.randomusers.data.mapper.DbDataMapper
import com.paloga.randomusers.domain.model.User
import com.paloga.randomusers.domain.repository.UsersRepository
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class GetFavorites(private val usersRepository: UsersRepository) {

    fun getFavorites(onFavoritesLoaded: (List<User>) -> Unit) {
        doAsync {
            val userList = loadFavorites()
            uiThread { onFavoritesLoaded.invoke(userList) }
        }
    }

    private fun loadFavorites(): List<User> {
        val dbDataMapper = DbDataMapper()
        return usersRepository.getUsers(false).map { dbDataMapper.convertToDomain(it) }.filter { it.isFavorite }.sortedBy { it.name }
    }
}