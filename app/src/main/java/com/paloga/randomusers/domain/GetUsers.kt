package com.paloga.randomusers.domain

import com.paloga.randomusers.data.mapper.DbDataMapper
import com.paloga.randomusers.domain.model.User
import com.paloga.randomusers.domain.repository.UsersRepository
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class GetUsers(private val usersRepository: UsersRepository) {

    fun getUsers(forceLoadFromApi: Boolean = false, onUsersLoaded: (List<User>) -> Unit) {
        doAsync {
            val userList = loadUsers(forceLoadFromApi)
            uiThread { onUsersLoaded.invoke(userList) }
        }
    }

    private fun loadUsers(forceLoadFromApi: Boolean): List<User> {
        val dbDataMapper = DbDataMapper()
        return usersRepository.getUsers(forceLoadFromApi).map { dbDataMapper.convertToDomain(it) }.sortedBy { it.name }
    }
}