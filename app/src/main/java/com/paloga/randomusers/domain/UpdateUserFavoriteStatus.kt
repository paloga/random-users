package com.paloga.randomusers.domain

import com.paloga.randomusers.data.mapper.DbDataMapper
import com.paloga.randomusers.domain.model.User
import com.paloga.randomusers.domain.repository.UsersRepository
import org.jetbrains.anko.doAsync

class UpdateUserFavoriteStatus(private val usersRepository: UsersRepository) {

    fun updateUserFavoriteStatus(user: User) {
        user.isFavorite = !user.isFavorite
        user.favorite = if (user.favorite == 1) 0 else 1
        doAsync {
            val dbDataMapper = DbDataMapper()
            usersRepository.updateUserFavoriteStatus(dbDataMapper.convertFromDomain(user))
        }
    }
}