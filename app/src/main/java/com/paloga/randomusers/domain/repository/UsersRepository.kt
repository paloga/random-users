package com.paloga.randomusers.domain.repository

import com.paloga.randomusers.data.entity.UserEntity

interface UsersRepository {

    fun getUsers(forceLoadFromApi: Boolean): List<UserEntity>

    fun updateUserFavoriteStatus(user: UserEntity)

    fun deleteUser(user: UserEntity)
}