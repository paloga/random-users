package com.paloga.randomusers.domain

import com.paloga.randomusers.data.mapper.DbDataMapper
import com.paloga.randomusers.domain.model.User
import com.paloga.randomusers.domain.repository.UsersRepository
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class DeleteUser(private val usersRepository: UsersRepository) {

    fun deleteUser(user: User, onDeleteCallBack: (User) -> Unit) {
        doAsync {
            val dbDataMapper = DbDataMapper()
            usersRepository.deleteUser(dbDataMapper.convertFromDomain(user))
            uiThread { onDeleteCallBack.invoke(user) }
        }
    }
}