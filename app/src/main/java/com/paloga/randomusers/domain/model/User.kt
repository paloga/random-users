package com.paloga.randomusers.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(override val _id: Long?,
                override val name: String,
                override val surname: String,
                override val gender: String,
                override val email: String,
                override val phoneNumber: String,
                override val photoUrl: String,
                override val street: String,
                override val city: String,
                override val state: String,
                override val registeredDate: String,
                override var favorite: Int,
                var isFavorite: Boolean = favorite == 1) : UserReadable, Parcelable {

    fun getCompleteName() = "${name.capitalize()} ${surname.capitalize()}"
}