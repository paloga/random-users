package com.paloga.randomusers.domain.model

interface UserReadable {
    val _id: Long?
    val name: String
    val surname: String
    val gender: String
    val email: String
    val phoneNumber: String
    val photoUrl: String
    val street: String
    val city: String
    val state: String
    val registeredDate: String
    val favorite: Int
}