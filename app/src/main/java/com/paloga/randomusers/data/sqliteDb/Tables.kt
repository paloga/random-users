package com.paloga.randomusers.data.sqliteDb


object UserTable {
    val TABLE_NAME = "User"
    val ID = "_id"
    val NAME = "name"
    val SURNAME = "surname"
    val GENDER = "gender"
    val EMAIL = "email"
    val PHONE_NUMBER = "phoneNumber"
    val PHOTO_URL = "photoUrl"
    val STREET = "street"
    val CITY = "city"
    val STATE = "state"
    val REGISTERED_DATE = "registeredDate"
    val FAVORITE = "favorite"
}