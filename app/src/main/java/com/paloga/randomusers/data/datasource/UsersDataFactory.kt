package com.paloga.randomusers.data.datasource

class UsersDataFactory {

    private val sqliteDataSource by lazy { SqliteUsersDataSource() }

    fun getReadableDataSource(forceLoadFromApi: Boolean): UsersDataSource {
        return if (!forceLoadFromApi && doWeHaveUsersInLocalSqliteDB()) {
            sqliteDataSource
        } else {
            RandomNamesDataSource()
        }
    }

    fun getWritableDataSource(): SqliteUsersDataSource = sqliteDataSource

    private fun doWeHaveUsersInLocalSqliteDB() = sqliteDataSource.getUserList().isNotEmpty()
}