package com.paloga.randomusers.data.mapper

import com.paloga.randomusers.data.entity.UserEntity
import com.paloga.randomusers.domain.model.User
import com.paloga.randomusers.domain.model.UserReadable

class DbDataMapper {

    fun convertFromDomain(user: UserReadable) = with(user) {
        UserEntity(_id, name, surname, gender, email, phoneNumber, photoUrl, street, city, state, registeredDate, favorite)
    }

    fun convertToDomain(user: UserReadable) = with(user) {
        User(_id, name, surname, gender, email, phoneNumber, photoUrl, street, city, state, registeredDate, favorite)
    }

}