package com.paloga.randomusers.data.datasource

import com.paloga.randomusers.commons.extensions.parseList
import com.paloga.randomusers.commons.extensions.toVarargArray
import com.paloga.randomusers.data.entity.UserEntity
import com.paloga.randomusers.data.mapper.DbDataMapper
import com.paloga.randomusers.data.sqliteDb.UserDbHelper
import com.paloga.randomusers.data.sqliteDb.UserTable
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.db.update

class SqliteUsersDataSource(private val userDbHelper: UserDbHelper = UserDbHelper.instance,
                            private val dataMapper: DbDataMapper = DbDataMapper()) : UsersDataSource {

    override fun getUserList(): List<UserEntity> {
        return userDbHelper.use {
            select(UserTable.TABLE_NAME).parseList { UserEntity(HashMap(it)) }
        }
    }

    fun saveUsers(users: List<UserEntity>) = userDbHelper.use {
        users.forEach {
            with(dataMapper.convertFromDomain(it)) {
                insert(UserTable.TABLE_NAME, *map.filterKeys { it != "_id" }.toVarargArray())
            }
        }
    }

    fun updateUserFavoriteStatus(user: UserEntity) = userDbHelper.use {
        with(dataMapper.convertFromDomain(user)) {
            //update(UserTable.TABLE_NAME, *map.toVarargArray()) // TODO [Paloga] I have to check why this is not working
            update(UserTable.TABLE_NAME, "favorite" to user.favorite)
                    .whereSimple("_id = ?", user._id.toString()).exec()
        }
    }

    fun deleteUser(user: UserEntity) = userDbHelper.use {
        with(dataMapper.convertFromDomain(user)) {
            delete(UserTable.TABLE_NAME, "_id = {_id}", *map.toVarargArray())
        }
    }
}