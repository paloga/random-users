package com.paloga.randomusers.data.entity

import com.google.gson.annotations.SerializedName

class RandomUsersResponse(
        @SerializedName("results") val users: List<RandomUser>
)