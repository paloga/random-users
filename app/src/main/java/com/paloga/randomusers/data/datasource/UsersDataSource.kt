package com.paloga.randomusers.data.datasource

import com.paloga.randomusers.data.entity.UserEntity

interface UsersDataSource {
    fun getUserList(): List<UserEntity>
}