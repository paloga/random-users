package com.paloga.randomusers.data.entity

class RandomUserLocation(val street: String,
                         val city: String,
                         val state: String)