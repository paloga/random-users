package com.paloga.randomusers.data.entity

import com.google.gson.annotations.SerializedName

class RandomUser(@SerializedName("name") val name: RandomUserName,
                 @SerializedName("location") val location: RandomUserLocation,
                 @SerializedName("gender") val gender: String,
                 @SerializedName("email") val email: String,
                 @SerializedName("phone")val phoneNumber: String,
                 @SerializedName("picture") val photoUrl: RandomUserPicture,
                 @SerializedName("registered") val registeredDate: String)