package com.paloga.randomusers.data.entity

class RandomUserPicture(val large: String,
                        val medium: String,
                        val thumbnail: String)