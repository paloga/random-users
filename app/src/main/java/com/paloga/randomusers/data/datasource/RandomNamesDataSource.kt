package com.paloga.randomusers.data.datasource

import com.paloga.randomusers.data.RandomUserService
import com.paloga.randomusers.data.entity.UserEntity
import com.paloga.randomusers.data.mapper.RandomUserMapper

class RandomNamesDataSource : UsersDataSource {

    private val randomUserService by lazy { RandomUserService.create() }

    override fun getUserList(): List<UserEntity> {
        val retrofitCall = randomUserService.getRandomUsers()
        val result = retrofitCall.execute().body()
        val users = RandomUserMapper().transform(result?.users ?: emptyList())
        return users.distinct() //Remove duplicates
    }

}