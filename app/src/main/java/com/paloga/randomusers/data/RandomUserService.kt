package com.paloga.randomusers.data

import com.paloga.randomusers.BuildConfig
import com.paloga.randomusers.data.entity.RandomUser
import com.paloga.randomusers.data.entity.RandomUsersResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface RandomUserService {

    @GET("/")
    fun getRandomUsers(@Query("results") results: Int = 40): Call<RandomUsersResponse>;

    companion object {
        fun create(): RandomUserService {

            val client = OkHttpClient().newBuilder()
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = if (BuildConfig.DEBUG) Level.BODY else Level.NONE
                    })
                    .build()

            val retrofit = Retrofit.Builder()
                    .baseUrl("https://api.randomuser.me/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return retrofit.create(RandomUserService::class.java)
        }
    }
}