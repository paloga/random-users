package com.paloga.randomusers.data.entity

import com.paloga.randomusers.domain.model.UserReadable

class UserEntity(val map: MutableMap<String, Any?>) : UserReadable {
    override var _id: Long? by map
    override var name: String by map
    override var surname: String by map
    override var gender: String by map
    override var email: String by map
    override var phoneNumber: String by map
    override var photoUrl: String by map
    override var street: String by map
    override var city: String by map
    override var state: String by map
    override var registeredDate: String by map
    override var favorite: Int by map

    constructor(id: Long? = null,
                name: String,
                surname: String,
                gender: String,
                email: String,
                phoneNumber: String,
                photoUrl: String,
                street: String,
                city: String,
                state: String,
                registeredDate: String,
                favorite: Int) : this(HashMap()) {
        this._id = id
        this.name = name
        this.surname = surname
        this.gender = gender
        this.email = email
        this.phoneNumber = phoneNumber
        this.photoUrl = photoUrl
        this.street = street
        this.city = city
        this.state = state
        this.registeredDate = registeredDate
        this.favorite = favorite
    }

    override fun toString(): String {
        return "name: $name, surname: $surname"
    }
}