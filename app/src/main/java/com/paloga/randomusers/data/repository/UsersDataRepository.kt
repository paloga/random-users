package com.paloga.randomusers.data.repository

import com.paloga.randomusers.data.datasource.UsersDataFactory
import com.paloga.randomusers.data.entity.UserEntity
import com.paloga.randomusers.domain.repository.UsersRepository
import org.jetbrains.anko.doAsync

class UsersDataRepository(private val usersDataFactory: UsersDataFactory) : UsersRepository {

    override fun getUsers(forceLoadFromApi: Boolean): List<UserEntity> {
        val readableNamesDataSource = usersDataFactory.getReadableDataSource(forceLoadFromApi)
        val writableNamesDataSource = usersDataFactory.getWritableDataSource()

        val users = if (readableNamesDataSource != writableNamesDataSource) {
            writableNamesDataSource.saveUsers(readableNamesDataSource.getUserList())
            //We need the users ids updated, so we are getting them from sqlitedb at this point
            writableNamesDataSource.getUserList()
        } else {
            readableNamesDataSource.getUserList()
        }
        return users
    }

    override fun updateUserFavoriteStatus(user: UserEntity) {
        usersDataFactory.getWritableDataSource().updateUserFavoriteStatus(user)
    }

    override fun deleteUser(user: UserEntity) {
        usersDataFactory.getWritableDataSource().deleteUser(user)
    }

}