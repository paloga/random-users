package com.paloga.randomusers.data.mapper

import com.paloga.randomusers.data.entity.RandomUser
import com.paloga.randomusers.data.entity.UserEntity

class RandomUserMapper {
    fun transform(randomUsers: List<RandomUser>): List<UserEntity> {
        return randomUsers.map { transform(it) }
    }

    private fun transform(randomUser: RandomUser) = randomUser.let {
        UserEntity(name = it.name.first,
                surname = it.name.last,
                gender = it.gender,
                email = it.email,
                phoneNumber = it.phoneNumber,
                photoUrl = it.photoUrl.large,
                street = it.location.street,
                city = it.location.city,
                state = it.location.state,
                registeredDate = it.registeredDate,
                favorite = 0)
    }
}