package com.paloga.randomusers.data.sqliteDb

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.paloga.randomusers.RandomUsersApp
import org.jetbrains.anko.db.*

class UserDbHelper(context: Context = RandomUsersApp.instance) : ManagedSQLiteOpenHelper(context,
        DB_NAME, null, DB_VERSION) {

    companion object {
        val DB_NAME = "random_users.db"
        val DB_VERSION = 1
        val instance by lazy { UserDbHelper() }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(UserTable.TABLE_NAME, true,
                UserTable.ID to INTEGER + PRIMARY_KEY,
                UserTable.NAME to TEXT,
                UserTable.SURNAME to TEXT,
                UserTable.GENDER to TEXT,
                UserTable.EMAIL to TEXT,
                UserTable.PHONE_NUMBER to TEXT,
                UserTable.PHOTO_URL to TEXT,
                UserTable.STREET to TEXT,
                UserTable.CITY to TEXT,
                UserTable.STATE to TEXT,
                UserTable.REGISTERED_DATE to TEXT,
                UserTable.FAVORITE to INTEGER)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Nothing to do here by the moment
    }

}