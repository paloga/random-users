package com.paloga.randomusers.presentation.favorites

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceActivity
import com.paloga.randomusers.R
import com.paloga.randomusers.commons.extensions.gone
import com.paloga.randomusers.commons.extensions.visible
import com.paloga.randomusers.data.datasource.UsersDataFactory
import com.paloga.randomusers.data.repository.UsersDataRepository
import com.paloga.randomusers.domain.GetFavorites
import com.paloga.randomusers.domain.model.User
import com.paloga.randomusers.presentation.custom_view.UserItemAnimator
import kotlinx.android.synthetic.main.activity_users.*
import kotlinx.android.synthetic.main.toolbar.*


// TODO [Paloga] We could make an kotlin interface with common parts between FavoritesActivity and UsersActivity
class FavoritesActivity : MvpLceActivity<RecyclerView, List<User>, FavoritesView, FavoritesPresenter>(), FavoritesView {

    private val favoritesAdapter by lazy {
        FavoritesAdapter(emptyList())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users)

        presenter.start()
    }

    override fun setData(userList: List<User>) {
        runOnUiThread {
            showLoading(false)
            favoritesAdapter.userList = userList
        }
    }

    override fun loadData(pullToRefresh: Boolean) {
        presenter.loadUsers()
    }

    override fun createPresenter() = FavoritesPresenter(GetFavorites(UsersDataRepository(UsersDataFactory())))

    override fun getErrorMessage(e: Throwable?, pullToRefresh: Boolean): String = getString(R.string.user_error)

    override fun initViews() {
        initToolbars()
        initRecyclerView()
    }

    private fun initToolbars() {
        initActionToolbar()
        layFavoriteCounter.visible()
    }

    private fun initActionToolbar() {
        val toolbar = toolbar as Toolbar?
        setSupportActionBar(toolbar)
        supportActionBar?.title = null
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar?.setNavigationIcon(R.drawable.ic_arrow_back)
    }

    private fun initRecyclerView() {
        contentView.layoutManager = LinearLayoutManager(this)
        contentView.adapter = favoritesAdapter
        contentView.itemAnimator = UserItemAnimator()
    }

    override fun updateFavoriteCounter() {
        val favoriteCount = favoritesAdapter.userList.count { it.isFavorite }
        tsLikesCounter.setCurrentText((favoriteCount - 1).toString())
        tsLikesCounter.setText(favoriteCount.toString())
    }

    override fun showLoading(pullToRefresh: Boolean) {
        super.showLoading(pullToRefresh)
        errorView.gone()
        contentView.gone()
        emptyView.gone()
    }

    override fun showContent() {
        super.showContent()
        emptyView.gone()
        errorView.gone()
        loadingView.gone()
    }

    override fun showEmptyView() {
        loadingView.gone()
        errorView.gone()
        contentView.gone()
        emptyView.visible()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }
}
