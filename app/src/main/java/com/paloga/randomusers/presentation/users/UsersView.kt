package com.paloga.randomusers.presentation.users

import android.widget.ImageView
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView
import com.paloga.randomusers.domain.model.User

interface UsersView : MvpLceView<List<User>> {

    fun initViews()

    fun updateFavoriteCounter(descending: Boolean = false)

    fun deleteUser(user: User)

    fun showEmptyView()

    fun showMoreData(users: List<User>)

    fun showUserDetail(user: User, imageViewClicked: ImageView)

    fun showSearchView()

    fun showFavoritesView()
}