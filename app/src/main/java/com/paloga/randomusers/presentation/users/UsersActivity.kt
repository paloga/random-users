package com.paloga.randomusers.presentation.users

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceActivity
import com.paloga.randomusers.R
import com.paloga.randomusers.commons.extensions.*
import com.paloga.randomusers.data.datasource.UsersDataFactory
import com.paloga.randomusers.data.repository.UsersDataRepository
import com.paloga.randomusers.domain.DeleteUser
import com.paloga.randomusers.domain.GetUsers
import com.paloga.randomusers.domain.UpdateUserFavoriteStatus
import com.paloga.randomusers.domain.model.User
import com.paloga.randomusers.presentation.custom_view.UserItemAnimator
import com.paloga.randomusers.presentation.custom_view.UsersSearchView.UsersSearchView
import com.paloga.randomusers.presentation.favorites.FavoritesActivity
import com.paloga.randomusers.presentation.user_detail.UserDetailActivity
import kotlinx.android.synthetic.main.activity_users.*
import kotlinx.android.synthetic.main.toolbar.*

class UsersActivity : MvpLceActivity<RecyclerView, List<User>, UsersView, UsersPresenter>(), UsersView {

    private lateinit var searchMenu: Menu
    private lateinit var searchItem: MenuItem

    private val usersSearchView by lazy { createUsersSearchView() }

    private val usersAdapter by lazy {
        UsersAdapter(emptyList(),
                photoClickListener = { user, imageViewClicked -> presenter.onUserPhotoClicked(user, imageViewClicked) },
                favoriteClickListener = { presenter.onFavoriteUserClicked(it) },
                deleteClickListener = { presenter.onDeleteUserClicked(it) },
                loadMoreClickListener = { presenter.onLoadMoreUsersClicked() })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users)

        presenter.start()
    }

    override fun setData(userList: List<User>) {
        runOnUiThread {
            showLoading(false)
            usersAdapter.userList = userList
        }
    }

    override fun loadData(pullToRefresh: Boolean) {
        presenter.loadUsers()
    }

    private fun createUsersSearchView() = searchItem.actionView as UsersSearchView

    override fun createPresenter(): UsersPresenter {
        val usersDataRepository = UsersDataRepository(UsersDataFactory())
        return UsersPresenter(GetUsers(usersDataRepository),
                UpdateUserFavoriteStatus(usersDataRepository),
                DeleteUser(usersDataRepository))
    }

    override fun getErrorMessage(e: Throwable?, pullToRefresh: Boolean): String = getString(R.string.user_error)

    override fun initViews() {
        initToolbars()
        initRecyclerView()
        usersSearchView.setQueryListenerToSearchView({ onSearchItem(it) })
    }

    private fun initToolbars() {
        initActionToolbar()
        initSearchToolbar()
        layFavoriteCounter.visible()
        layFavoriteCounter.setOnClickListener { presenter.onFavoritesClicked() }
    }

    private fun initActionToolbar() {
        val toolbar = toolbar as Toolbar?
        setSupportActionBar(toolbar)
        supportActionBar?.title = null
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar?.setNavigationIcon(R.drawable.ic_close)
    }

    private fun initSearchToolbar() {
        val searchToolbar = (searchToolbar as Toolbar)
        searchToolbar.inflateMenu(R.menu.menu_search)
        searchMenu = searchToolbar.menu
        searchItem = searchMenu.findItem(R.id.actionSearch)

        searchToolbar.setNavigationOnClickListener({
            hideSearchToolbar()
            usersSearchView.clearSearchText()
        })

        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                hideSearchToolbar()
                usersSearchView.clearSearchText()
                return true
            }

            override fun onMenuItemActionExpand(item: MenuItem) = true
        })
    }

    private fun initRecyclerView() {
        contentView.layoutManager = LinearLayoutManager(this)
        contentView.adapter = usersAdapter
        contentView.itemAnimator = UserItemAnimator()
    }

    override fun updateFavoriteCounter(descending: Boolean) {
        val favoriteCount = usersAdapter.userList.count { it.isFavorite }
        if (descending) {
            tsLikesCounter.setCurrentText((favoriteCount + 1).toString())
        } else {
            tsLikesCounter.setCurrentText((favoriteCount - 1).toString())
        }
        tsLikesCounter.setText(favoriteCount.toString())
    }

    override fun deleteUser(user: User) {
        val usersListAfterDeleting = usersAdapter.userList.toMutableList()
        val positionDeleted = usersListAfterDeleting.indexOf(user)
        usersListAfterDeleting.removeAt(positionDeleted)
        usersAdapter.userList = usersListAfterDeleting
        usersAdapter.notifyItemRemoved(positionDeleted)
    }

    override fun showLoading(pullToRefresh: Boolean) {
        super.showLoading(pullToRefresh)
        errorView.gone()
        contentView.gone()
        emptyView.gone()
    }

    override fun showContent() {
        super.showContent()
        emptyView.gone()
        errorView.gone()
        loadingView.gone()
    }

    override fun showMoreData(users: List<User>) {
        val firstItemInsertedPosition = usersAdapter.userList.size
        val totalUsers = usersAdapter.userList + users
        usersAdapter.userList = totalUsers
        usersAdapter.notifyItemRangeInserted(firstItemInsertedPosition, users.size - 1)
        contentView.scrollToPosition(usersAdapter.userList.size)
    }

    override fun showEmptyView() {
        loadingView.gone()
        errorView.gone()
        contentView.gone()
        emptyView.visible()
    }

    override fun showUserDetail(user: User, imageViewClicked: ImageView) {
        val extras = Bundle()
        extras.putParcelable(UserDetailActivity.Companion.Extras.USER, user)
        openActivityWithSharedTransitionPhoto(UserDetailActivity::class.java, extras, imageViewClicked)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_users, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.actionSearch -> {
                presenter.onSearchClicked()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showSearchView() {
        showSearchToolbar()
        searchItem.expandActionView()
    }

    private fun hideSearchToolbar() {
        searchToolbar.circleReveal(posFromRight = 1, containsOverflow = true, isShow = false)
    }

    private fun showSearchToolbar() {
        searchToolbar.circleReveal(posFromRight = 1, containsOverflow = true, isShow = true)
    }

    private fun onSearchItem(item: String) = findUserInList(item)

    private fun findUserInList(text: String) {
        (contentView.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(usersAdapter.getFirstItemPositionStartingWith(text), 20)
    }

    override fun showFavoritesView() {
        openActivity(FavoritesActivity::class.java)
    }
}
