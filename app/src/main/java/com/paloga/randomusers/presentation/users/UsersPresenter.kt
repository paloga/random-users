package com.paloga.randomusers.presentation.users

import android.widget.ImageView
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.paloga.randomusers.domain.DeleteUser
import com.paloga.randomusers.domain.GetUsers
import com.paloga.randomusers.domain.UpdateUserFavoriteStatus
import com.paloga.randomusers.domain.model.User
import timber.log.Timber

class UsersPresenter(private val getUsersUseCase: GetUsers,
                     private val updateUserFavoriteStatusUseCase: UpdateUserFavoriteStatus,
                     private val deleteUserUseCase: DeleteUser) : MvpBasePresenter<UsersView>() {

    fun start() {
        ifViewAttached {
            it.initViews()
            it.showLoading(false)
            it.loadData(false)
        }
    }

    fun loadUsers() {
        Timber.d("Loading users...")
        getUsersUseCase.getUsers { onUsersLoaded(it) }
    }

    private fun onUsersLoaded(users: List<User>) {
        Timber.d("onUsersLoaded: ${users.size} users")
        ifViewAttached {
            if (users.isNotEmpty()) {
                it.setData(users)
                it.showContent()
                it.updateFavoriteCounter()
            } else {
                it.showEmptyView()
            }
        }
    }

    fun onFavoriteUserClicked(user: User) {
        Timber.d("User ${user.name} favorite clicked")
        updateUserFavoriteStatusUseCase.updateUserFavoriteStatus(user)
        ifViewAttached { it.updateFavoriteCounter() }
    }

    fun onFavoritesClicked() {
        ifViewAttached { it.showFavoritesView() }
    }

    fun onLoadMoreUsersClicked() {
        Timber.d("Loading more users...")
        getUsersUseCase.getUsers(true, { onMoreUsersLoaded(it) })
    }

    private fun onMoreUsersLoaded(users: List<User>) {
        Timber.d("onMoreUsersLoaded: ${users.size} more users loaded")
        ifViewAttached {
            if (users.isNotEmpty()) {
                it.showMoreData(users)
            }
        }
    }

    fun onDeleteUserClicked(user: User) {
        Timber.d("User ${user.name} delete clicked")
        deleteUserUseCase.deleteUser(user, { onDeletedUser(it) })
    }

    private fun onDeletedUser(user: User) {
        ifViewAttached {
            it.deleteUser(user)
            it.updateFavoriteCounter()
        }
    }

    // [Paloga] : I prefer not to have any Android dependency in presenters, but here, I did an exception with ImageView
    fun onUserPhotoClicked(user: User, imageViewClicked: ImageView) {
        ifViewAttached { it.showUserDetail(user, imageViewClicked) }
    }

    fun onSearchClicked() {
        ifViewAttached { it.showSearchView() }
    }
}