package com.paloga.randomusers.presentation.users

import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.mikhaellopez.hfrecyclerview.HFRecyclerView
import com.paloga.randomusers.R
import com.paloga.randomusers.commons.extensions.loadRoundedImage
import com.paloga.randomusers.domain.model.User
import kotlinx.android.synthetic.main.row_load_more_users.view.*
import kotlinx.android.synthetic.main.row_user.view.*

class UsersAdapter(var userList: List<User>,
                   private val photoClickListener: (User, ImageView) -> Unit,
                   private val favoriteClickListener: (User) -> Unit,
                   private val deleteClickListener: (User) -> Unit,
                   private val loadMoreClickListener: () -> Unit) : HFRecyclerView<User>(userList, false, true) {

    companion object {
        const val ACTION_LIKE_BUTTON_CLICKED = "action_like_button_button"
        const val ACTION_UNLIKE_BUTTON_CLICKED = "action_unlike_button_button"
    }

    inner class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindUser(user: User) = with(itemView) {
            txtNameTitle.text = user.getCompleteName()
            txtEmail.text = user.email
            txtPhoneNumber.text = user.phoneNumber
            ivUserPhoto.loadRoundedImage(user.photoUrl)
            ViewCompat.setTransitionName(ivUserPhoto, user.getCompleteName())

            if (user.isFavorite) {
                ivLike.setImageResource(R.drawable.ic_heart_red)
            } else {
                ivLike.setImageResource(R.drawable.ic_heart_outline_grey)
            }
            ivLike.setOnClickListener {
                if (user.isFavorite) {
                    notifyItemChanged(adapterPosition, ACTION_UNLIKE_BUTTON_CLICKED)
                } else {
                    notifyItemChanged(adapterPosition, ACTION_LIKE_BUTTON_CLICKED)
                }
                favoriteClickListener(user)
            }
            ivDelete.setOnClickListener {
                deleteClickListener(user)
            }
            ivUserPhoto.setOnClickListener {
                photoClickListener(user, ivUserPhoto)
            }
        }
    }

    inner class FooterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindFooter(loadMoreClickListener: () -> Unit) = with(itemView) {
            btnLoadMore.setOnClickListener { loadMoreClickListener() }
        }
    }

    override fun getItemCount() = userList.size + 1 // +1 because of footer

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is UserViewHolder -> holder.bindUser(userList[position])
            is FooterViewHolder -> holder.bindFooter(loadMoreClickListener)
        }
    }

    override fun getHeaderView(inflater: LayoutInflater, parent: ViewGroup?): RecyclerView.ViewHolder? {
        // Nothing to do here
        return null
    }

    override fun getFooterView(inflater: LayoutInflater, parent: ViewGroup?): RecyclerView.ViewHolder {
        return FooterViewHolder(inflater.inflate(R.layout.row_load_more_users, parent, false))
    }

    override fun getItemView(inflater: LayoutInflater, parent: ViewGroup?): RecyclerView.ViewHolder {
        return UserViewHolder(inflater.inflate(R.layout.row_user, parent, false))
    }

    fun getFirstItemPositionStartingWith(text: String): Int {
        val namePosition = userList.indexOfFirst {
            it.name.toLowerCase().startsWith(text.toLowerCase())
        }
        val surnamePosition = userList.indexOfFirst {
            it.surname.toLowerCase().startsWith(text.toLowerCase())
        }
        val emailPosition = userList.indexOfFirst {
            it.email.toLowerCase().startsWith(text.toLowerCase())
        }
        return Math.max(Math.max(namePosition, surnamePosition), emailPosition)
    }
}