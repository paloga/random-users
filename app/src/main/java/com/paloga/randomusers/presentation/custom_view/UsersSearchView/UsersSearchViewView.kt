package com.paloga.randomusers.presentation.custom_view.UsersSearchView

import com.hannesdorfmann.mosby3.mvp.MvpView

interface UsersSearchViewView : MvpView {

    fun initSearchView()

    fun doSearch(query: String, searchCallback: (String) -> Unit)

    fun clearFocus()

    fun clearSearchText()
}