package com.paloga.randomusers.presentation.favorites

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mikhaellopez.hfrecyclerview.HFRecyclerView
import com.paloga.randomusers.R
import com.paloga.randomusers.commons.extensions.loadRoundedImage
import com.paloga.randomusers.domain.model.User
import kotlinx.android.synthetic.main.row_user.view.*

class FavoritesAdapter(var userList: List<User>) : HFRecyclerView<User>(userList, false, false) {

    inner class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindUser(user: User) = with(itemView) {
            txtNameTitle.text = user.getCompleteName()
            txtEmail.text = user.email
            txtPhoneNumber.text = user.phoneNumber
            ivUserPhoto.loadRoundedImage(user.photoUrl)
        }
    }

    override fun getItemCount() = userList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is UserViewHolder -> holder.bindUser(userList[position])
        }
    }

    override fun getHeaderView(inflater: LayoutInflater, parent: ViewGroup?): RecyclerView.ViewHolder? {
        // Nothing to do here
        return null
    }

    override fun getFooterView(inflater: LayoutInflater, parent: ViewGroup?): RecyclerView.ViewHolder? {
        // Nothing to do here
        return null
    }

    override fun getItemView(inflater: LayoutInflater, parent: ViewGroup?): RecyclerView.ViewHolder {
        return UserViewHolder(inflater.inflate(R.layout.row_favorite, parent, false))
    }
}