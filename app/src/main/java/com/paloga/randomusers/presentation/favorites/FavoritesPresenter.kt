package com.paloga.randomusers.presentation.favorites

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.paloga.randomusers.domain.GetFavorites
import com.paloga.randomusers.domain.model.User
import timber.log.Timber

class FavoritesPresenter(private val getFavoritesUseCase: GetFavorites) : MvpBasePresenter<FavoritesView>() {

    fun start() {
        ifViewAttached {
            it.initViews()
            it.showLoading(false)
            it.loadData(false)
        }
    }

    fun loadUsers() {
        Timber.d("Loading favorites...")
        getFavoritesUseCase.getFavorites { onFavoritesLoaded(it) }
    }

    private fun onFavoritesLoaded(users: List<User>) {
        Timber.d("onFavoritesLoaded: ${users.size} favorites")
        ifViewAttached {
            if (users.isNotEmpty()) {
                it.setData(users)
                it.showContent()
                it.updateFavoriteCounter()
            } else {
                it.showEmptyView()
            }
        }
    }
}