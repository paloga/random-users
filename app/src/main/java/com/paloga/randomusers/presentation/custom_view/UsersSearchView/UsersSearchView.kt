package com.paloga.randomusers.presentation.custom_view.UsersSearchView

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.SearchView
import android.util.AttributeSet
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.paloga.randomusers.R
import timber.log.Timber

class UsersSearchView :
        MvpSearchView<UsersSearchViewView, UsersSearchViewPresenter>,
        UsersSearchViewView {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun createPresenter() = UsersSearchViewPresenter()

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        presenter.start()
    }

    override fun initSearchView() {
        setBackgroundResource(R.drawable.abc_textfield_search_default_mtrl_alpha)
        maxWidth = Integer.MAX_VALUE
        isSubmitButtonEnabled = false

        setSearchViewCloseButton()
        setSearchViewTextProperties()
        setSearchViewCursor()
    }

    private fun setSearchViewCloseButton() {
        val closeButton = findViewById<ImageView>(R.id.search_close_btn)
        closeButton.setImageResource(R.drawable.ic_close)
    }

    private fun setSearchViewTextProperties() {
        val txtSearch = findViewById<EditText>(android.support.v7.appcompat.R.id.search_src_text)
        txtSearch.setHint(R.string.search_hint)
        txtSearch.setHintTextColor(Color.LTGRAY)
        txtSearch.setTextColor(Color.WHITE)
    }

    private fun setSearchViewCursor() {
        val searchTextView = findViewById<AutoCompleteTextView>(android.support.v7.appcompat.R.id.search_src_text)

        try {
            val cursorDrawableRes = TextView::class.java.getDeclaredField("mCursorDrawableRes")
            cursorDrawableRes.isAccessible = true
            cursorDrawableRes.set(searchTextView, R.drawable.search_cursor) //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (e: Exception) {
            Timber.e(e, "setSearchViewCursor error")
        }
    }

    fun setQueryListenerToSearchView(searchEvent: (String) -> Unit) {
        setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                presenter.onQueryTextSubmit(query, searchEvent)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                presenter.onQueryTextChange(newText, searchEvent)
                return true
            }
        })
    }

    override fun doSearch(query: String, searchCallback: (String) -> Unit) {
        Timber.i("query $query")
        if (!query.isEmpty()) searchCallback.invoke(query)
    }

    override fun clearSearchText() {
        val searchTextView = findViewById<AutoCompleteTextView>(android.support.v7.appcompat.R.id.search_src_text)
        searchTextView.setText("")
    }
}