package com.paloga.randomusers.presentation.custom_view.UsersSearchView

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.paloga.randomusers.presentation.custom_view.UsersSearchView.UsersSearchViewView

class UsersSearchViewPresenter() : MvpBasePresenter<UsersSearchViewView>() {

    fun start() = ifViewAttached { it.initSearchView() }

    fun onQueryTextSubmit(query: String, searchCallback: (String) -> Unit) {
        ifViewAttached{
            it.doSearch(query, searchCallback)
            it.clearFocus()
        }
    }

    fun onQueryTextChange(query: String, searchCallback: (String) -> Unit) {
        ifViewAttached{
            it.doSearch(query, searchCallback)
        }
    }
}
