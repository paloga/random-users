package com.paloga.randomusers.presentation.user_detail

import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.View
import android.view.animation.AlphaAnimation
import com.paloga.randomusers.R
import com.paloga.randomusers.commons.extensions.loadRoundedImageWithSharedTransition
import com.paloga.randomusers.domain.model.User
import kotlinx.android.synthetic.main.activity_user_detail.*


// TODO [Paloga] MVP
class UserDetailActivity : AppCompatActivity(), AppBarLayout.OnOffsetChangedListener {

    companion object {
        private const val PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f
        private const val PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.3f
        private const val ALPHA_ANIMATIONS_DURATION = 200

        object Extras {
            const val USER = "user_item"
        }
    }

    private var isNameVisible = false
    private var isNameContainerVisible = true

    private lateinit var menuUserDetail: Menu
    private val user: User by lazy { intent.extras.getParcelable(Extras.USER) as User }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)

        supportPostponeEnterTransition()
        appBarUserDetail.addOnOffsetChangedListener(this)
        startAlphaAnimation(txtNameTitle, 0, View.INVISIBLE)

        loadUserDetails()
    }

    private fun loadUserDetails() {
        txtNameTitle.text = user.getCompleteName()
        txtName.text = user.getCompleteName()
        txtGender.text = user.gender.capitalize()
        txtStreet.text = user.street.capitalize()
        txtCity.text = user.city.capitalize()
        txtState.text = user.state.capitalize()
        txtRegisteredDate.text = user.registeredDate
        txtEmail.text = user.email

        if (user.isFavorite){
            toolbar.inflateMenu(R.menu.menu_user_detail)
        }

        ivUserDetailPhoto.transitionName = intent.extras.getString("user_photo_transition")
        ivUserDetailPhoto.loadRoundedImageWithSharedTransition(user.photoUrl, { supportStartPostponedEnterTransition() })
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
        val maxScroll = appBarLayout.totalScrollRange
        val percentage = Math.abs(verticalOffset).toFloat() / maxScroll.toFloat()

        handleAlphaOnTitle(percentage)
        handleToolbarTitleVisibility(percentage)
    }

    private fun handleAlphaOnTitle(percentage: Float) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (isNameContainerVisible) {
                startAlphaAnimation(layUserDetailName, ALPHA_ANIMATIONS_DURATION.toLong(), View.INVISIBLE)
                isNameContainerVisible = false
            }

        } else {

            if (!isNameContainerVisible) {
                startAlphaAnimation(layUserDetailName, ALPHA_ANIMATIONS_DURATION.toLong(), View.VISIBLE)
                isNameContainerVisible = true
            }
        }
    }

    private fun handleToolbarTitleVisibility(percentage: Float) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {

            if (!isNameVisible) {
                startAlphaAnimation(txtNameTitle, ALPHA_ANIMATIONS_DURATION.toLong(), View.VISIBLE)
                isNameVisible = true
            }

        } else {

            if (isNameVisible) {
                startAlphaAnimation(txtNameTitle, ALPHA_ANIMATIONS_DURATION.toLong(), View.INVISIBLE)
                isNameVisible = false
            }
        }
    }

    private fun startAlphaAnimation(v: View, duration: Long, visibility: Int) {
        val alphaAnimation = if (visibility == View.VISIBLE)
            AlphaAnimation(0f, 1f)
        else
            AlphaAnimation(1f, 0f)

        alphaAnimation.duration = duration
        alphaAnimation.fillAfter = true
        v.startAnimation(alphaAnimation)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuUserDetail = menu
        menuInflater.inflate(R.menu.menu_user_detail, menu)
        return true
    }
}