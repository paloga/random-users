package com.paloga.randomusers.commons.extensions

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.paloga.randomusers.R

fun ImageView.loadRoundedImage(imageUrl: String) {
    Glide.with(this.context)
            .load(imageUrl)
            .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_users))
            .into(this)
}

fun ImageView.loadRoundedImageWithSharedTransition(imageUrl: String, callback: () -> Unit) {
    Glide.with(this.context)
            .load(imageUrl)
            .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_users))
            .listener(object : RequestListener<Drawable> {
                override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                    callback.invoke()
                    return false
                }

                override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
                    callback.invoke()
                    return false
                }
            })
            .into(this)
}

