package com.paloga.randomusers

import android.app.Application
import com.paloga.randomusers.commons.extensions.DelegatesExt
import timber.log.Timber

class RandomUsersApp : Application() {

    companion object {
        var instance: RandomUsersApp by DelegatesExt.notNullSingleValue()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}