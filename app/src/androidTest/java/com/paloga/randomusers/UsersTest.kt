package com.paloga.randomusers

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.paloga.randomusers.CustomAssertions.Companion.hasItemCount
import com.paloga.randomusers.presentation.users.UsersActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UsersTest {
    @Rule
    @JvmField
    val activity = ActivityTestRule<UsersActivity>(UsersActivity::class.java)

    @Test
    fun countUsers() {
        onView(withId(R.id.contentView))
                .check(hasItemCount(41))

    }
}
