package com.paloga.randomusers

import com.paloga.randomusers.data.repository.UsersDataRepository
import com.paloga.randomusers.domain.DeleteUser
import com.paloga.randomusers.domain.GetUsers
import com.paloga.randomusers.domain.UpdateUserFavoriteStatus
import com.paloga.randomusers.presentation.users.UsersPresenter
import com.paloga.randomusers.presentation.users.UsersView
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UsersPresenterTest {
    @Mock
    lateinit var usersView: UsersView

    @Mock
    lateinit var getUsersUseCase: GetUsers
    @Mock
    lateinit var updateUserFavoriteStatusUseCase: UpdateUserFavoriteStatus
    @Mock
    lateinit var deleteUserUseCase: DeleteUser

    @Mock
    lateinit var usersDataRepository: UsersDataRepository

    lateinit var usersPresenter: UsersPresenter

    @Before
    fun setUp() {
        getUsersUseCase = GetUsers(usersDataRepository)
        updateUserFavoriteStatusUseCase = UpdateUserFavoriteStatus(usersDataRepository)
        deleteUserUseCase = DeleteUser(usersDataRepository)
        usersPresenter = UsersPresenter(getUsersUseCase, updateUserFavoriteStatusUseCase, deleteUserUseCase)
    }

    @Test
    fun testStart() {
        // When
        usersPresenter.start()
        usersPresenter.attachView(usersView)

        // Then
        Mockito.verify(usersView).initViews()
    }
}
